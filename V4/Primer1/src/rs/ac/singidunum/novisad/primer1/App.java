package rs.ac.singidunum.novisad.primer1;

import java.util.ArrayList;

import rs.ac.singidunum.novisad.primer1.model.Osoba;

public class App {

	public static void main(String[] args) {
		ArrayList<Osoba> osobe = new ArrayList<Osoba>();
		osobe.add(new Osoba("Marko", "Markovic"));
		osobe.add(new Osoba("Ivana", "Jovanovic"));
		osobe.add(new Osoba("Jovan", "Petrovic"));
		
		for(Osoba o : osobe) {
			o.start();
		}
	}
}
