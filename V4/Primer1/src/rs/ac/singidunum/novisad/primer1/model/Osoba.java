package rs.ac.singidunum.novisad.primer1.model;

public class Osoba extends Thread {
	private String ime;
	private String prezime;

	public Osoba() {
		super();
	}

	public Osoba(String ime, String prezime) {
		super();
		this.ime = ime;
		this.prezime = prezime;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	@Override
	public void run() {
		System.out.println(this.getName() + " " + this.ime + " " + this.prezime);
	}
}
