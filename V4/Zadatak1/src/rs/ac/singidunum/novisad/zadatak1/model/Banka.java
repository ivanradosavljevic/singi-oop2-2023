package rs.ac.singidunum.novisad.zadatak1.model;

public class Banka {
	public double sredstva;

	public Banka() {
		super();
	}

	public Banka(double sredstva) {
		super();
		this.sredstva = sredstva;
	}

	public double getSredstva() {
		return sredstva;
	}

	public void setSredstva(double sredstva) {
		this.sredstva = sredstva;
	}
	
	public void uplati(double iznos) {
		this.sredstva += iznos;
	}
	
	public double isplati(double iznos) {
		this.sredstva -= iznos;
		return iznos;
	}
}
