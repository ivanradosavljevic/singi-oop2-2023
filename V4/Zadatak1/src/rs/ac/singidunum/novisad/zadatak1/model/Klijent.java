package rs.ac.singidunum.novisad.zadatak1.model;

public class Klijent implements Runnable {
	private Banka banka;

	public Klijent() {
		super();
	}

	public Klijent(Banka banka) {
		super();
		this.banka = banka;
	}

	@Override
	public void run() {
		synchronized (banka) {
			for (int i = 0; i < 1000; i++) {
				this.banka.uplati(1000);
				this.banka.isplati(1000);
			}
			System.out.println(Thread.currentThread().getName() + " " + this.banka.getSredstva());
		}
	}

}
