package rs.ac.singidunum.novisad.zadatak2;

import java.util.ArrayList;

import rs.ac.singidunum.novisad.zadatak2.model.Filozof;
import rs.ac.singidunum.novisad.zadatak2.model.Viljuska;

public class App {

	public static void main(String[] args) throws InterruptedException {
		int brojFilozofa = 5;
		ArrayList<Thread> filozofi = new ArrayList<Thread>();
		ArrayList<Viljuska> viljuske = new ArrayList<Viljuska>();
		
		for (int i = 0; i < brojFilozofa; i++) {
			viljuske.add(new Viljuska());
		}

		for (int i = 0; i < brojFilozofa; i++) {
			if(i == brojFilozofa-1) {
				filozofi.add(new Thread(new Filozof(viljuske.get((i + 1) % viljuske.size()), viljuske.get(i))));
			} else {
				filozofi.add(new Thread(new Filozof(viljuske.get(i), viljuske.get((i + 1) % viljuske.size()))));
			}			
		}
		
		for(Thread f : filozofi) {
			f.start();
		}
	}

}
