package rs.ac.singidunum.novisad.primer2.model;

public class Klijent extends Thread {
	private Banka banka;

	public Klijent() {
		super();
	}

	public Klijent(Banka banka) {
		super();
		this.banka = banka;
	}

	@Override
	public void run() {
		for (int i = 0; i < 1000; i++) {
			this.banka.uplati(1000);
			this.banka.isplati(1000);
		}
		System.out.println(this.getName() + " " + this.banka.getSredstva());
	}

}
