package rs.ac.singidunum.novisad.primer2;

import java.util.ArrayList;

import rs.ac.singidunum.novisad.primer2.model.Banka;
import rs.ac.singidunum.novisad.primer2.model.Klijent;

public class App {

	public static void main(String[] args) {
		Banka banka = new Banka(10000);
		ArrayList<Klijent> klijenti = new ArrayList<Klijent>();
		for(int i = 0; i < 10; i++) {
			klijenti.add(new Klijent(banka));
		}
		System.out.println(banka.getSredstva());
		for(Klijent k : klijenti) {
			k.start();
		}
//		System.out.println(banka.getSredstva());
	}

}
