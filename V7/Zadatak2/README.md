# Zadatak 2

Napraviti implementaciju kalkulatora. Kalkulator treba da podržava osnovne matematičke operacije: sabiranje, oduzimanje, množenje i deljenje. Omogućiti proširivost uvođenjem mogućnosti za primenu proizvoljnih binarnih i unarnih operacija nad operandima. U kalkulatoru omogućiti pamćenje istorije poziva operacija i njihovih rezultata.