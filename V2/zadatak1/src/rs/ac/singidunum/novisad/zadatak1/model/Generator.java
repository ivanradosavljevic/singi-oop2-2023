package rs.ac.singidunum.novisad.zadatak1.model;

public interface Generator {
	public String generisi(Parametar parametar);
	public String generisi(Atribut atribut);
	public String generisi(Metoda metoda);
	public String generisi(Interfejs interfejs);
	public String generisi(Klasa klasa);
}
