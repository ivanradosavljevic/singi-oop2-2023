package rs.ac.singidunum.novisad.zadatak1.model;

import java.util.ArrayList;

public abstract class SlozeniTip extends VidljiviElement {
	private ArrayList<SlozeniTip> nasledjuje = new ArrayList<SlozeniTip>();
	private ArrayList<Atribut> atributi = new ArrayList<Atribut>();
	private ArrayList<Metoda> metode = new ArrayList<Metoda>();

	public SlozeniTip() {
		super();
	}
	
	public SlozeniTip(String naziv, String vidljivost) {
		super(naziv, vidljivost);
	}

	public SlozeniTip(String naziv, String vidljivost, ArrayList<SlozeniTip> nasledjuje, ArrayList<Atribut> atributi,
			ArrayList<Metoda> metode) {
		super(naziv, vidljivost);
		this.nasledjuje = nasledjuje;
		this.atributi = atributi;
		this.metode = metode;
	}

	public ArrayList<SlozeniTip> getNasledjuje() {
		return nasledjuje;
	}

	public void setNasledjuje(ArrayList<SlozeniTip> nasledjuje) {
		this.nasledjuje = nasledjuje;
	}

	public ArrayList<Atribut> getAtributi() {
		return atributi;
	}

	public void setAtributi(ArrayList<Atribut> atributi) {
		this.atributi = atributi;
	}

	public ArrayList<Metoda> getMetode() {
		return metode;
	}

	public void setMetode(ArrayList<Metoda> metode) {
		this.metode = metode;
	}
	
	public void dodajNadtip(SlozeniTip slozeniTip) {
		if(nasledjuje.contains(slozeniTip)) {
			throw new IllegalArgumentException("Prosledjeni tip je vec jednom dodat!");
		}
		nasledjuje.add(slozeniTip);
	}
	
	public SlozeniTip ukloniNadtip(int indeks) {
		return nasledjuje.remove(indeks);
	}
	
	public void dodajAtribut(Atribut atribut) {
		if(atributi.contains(atribut)) {
			throw new IllegalArgumentException("Prosledjeni atribut je vec jednom dodat!");
		}
		atributi.add(atribut);
	}
	
	public Atribut ukloniAtribut(int indeks) {
		return atributi.remove(indeks);
	}
	
	public void dodajMetodu(Metoda metoda) {
		if(metode.contains(metoda)) {
			throw new IllegalArgumentException("Prosledjena metoda je vec jednom dodata!");
		}
		metode.add(metoda);
	}
	
	public Metoda ukloniMetodu(int indeks) {
		return metode.remove(indeks);
	}
}
