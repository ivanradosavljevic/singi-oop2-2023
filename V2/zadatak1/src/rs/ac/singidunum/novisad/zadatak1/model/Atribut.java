package rs.ac.singidunum.novisad.zadatak1.model;

public class Atribut extends VidljiviElement {
	private boolean konstanta;
	private boolean staticki;
	private String tip;
	private String pocetnaVrednost;

	public Atribut() {
		super();
	}

	public Atribut(String naziv, String vidljivost, boolean konstanta, boolean staticki, String tip,
			String pocetnaVrednost) {
		super(naziv, vidljivost);
		this.konstanta = konstanta;
		this.staticki = staticki;
		this.tip = tip;
		this.pocetnaVrednost = pocetnaVrednost;
	}

	public boolean isKonstanta() {
		return konstanta;
	}

	public void setKonstanta(boolean konstanta) {
		this.konstanta = konstanta;
	}

	public boolean isStaticki() {
		return staticki;
	}

	public void setStaticki(boolean staticki) {
		this.staticki = staticki;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public String getPocetnaVrednost() {
		return pocetnaVrednost;
	}

	public void setPocetnaVrednost(String pocetnaVrednost) {
		this.pocetnaVrednost = pocetnaVrednost;
	}
}
