package rs.ac.singidunum.novisad.zadatak1.model;

public class Klasa extends SlozeniTip {
	private boolean apstratka = false;

	public Klasa() {
		super();
	}

	public Klasa(String naziv, String vidljivost) {
		super(naziv, vidljivost);
	}
	
	public Klasa(String naziv, String vidljivost, boolean apstratkna) {
		super(naziv, vidljivost);
		this.apstratka = apstratkna;
	}

	public boolean isApstratka() {
		return apstratka;
	}

	public void setApstratka(boolean apstratka) {
		this.apstratka = apstratka;
	}
}
