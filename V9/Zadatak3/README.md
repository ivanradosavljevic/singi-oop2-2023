# Zadatak 3
1. U paletu alata dodati dugme za zapisavanje sadržaja tabele radnika u CSV datoteku.
2. U paletu alata dodati dugme za učiavanje sadržaja CSV datoteke u program.
3. U podnožju programa dodati prikaz sumarne statistike za tabelu radnika. Sumarna statistika treba da sadrži broj registrovanih radnika, minimalnu, maksimalnu, sumu i prosečnu platu.
4. Dodati komponentu za prikaz suma plata po zanimanjima. Ova statistika se prikazuje kao pita podeljena na onoliko delova koliko ima zvanja pri čemu je veličina svakog dela proporcionalna sumi plata za posmatrano zanimanje.
5. U paletu alata dodati formu za pretragu. Forma treba da sadrži polje za unos teksta i dugme za pretragu. Pretraga se vrši po imenu i prezimenu radnika, pri čemu ime i prezime ne moraju biti uneti u potpunosti.