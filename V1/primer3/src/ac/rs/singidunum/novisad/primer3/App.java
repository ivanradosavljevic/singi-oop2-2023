package ac.rs.singidunum.novisad.primer3;

import java.util.ArrayList;

import ac.rs.singidunum.novisad.primer3.model.Nastavnik;
import ac.rs.singidunum.novisad.primer3.model.Osoba;
import ac.rs.singidunum.novisad.primer3.model.Radnik;
import ac.rs.singidunum.novisad.primer3.model.Sluzbenik;
import ac.rs.singidunum.novisad.primer3.model.Student;

public class App {
	public static void main(String[] args) {
		Osoba osoba = new Osoba("Marko", "Marković");
		Osoba student = new Student("Petar", "Petrović", "2021123456");
		
		ArrayList<Radnik> radnici = new ArrayList<Radnik>();
		radnici.add(new Nastavnik("Miodrag", "Živković", "P001", "Profesor"));
		radnici.add(new Sluzbenik("Marija", "Marijanović", "S001"));
		
		osoba.prikaz();
		for(Radnik r : radnici) {
			r.prikaz();
		}
		student.prikaz();
	}
}
