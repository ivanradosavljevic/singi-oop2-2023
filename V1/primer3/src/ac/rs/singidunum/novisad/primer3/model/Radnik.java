package ac.rs.singidunum.novisad.primer3.model;

public abstract class Radnik extends Osoba {
	protected String sifraZaposlenog;

	public Radnik() {
		super();
	}
	public Radnik(String ime, String prezime, String sifraZaposlenog) {
		super(ime, prezime);
		this.sifraZaposlenog = sifraZaposlenog;
	}
	@Override
	public void prikaz(String krajReda) {
		System.out.print(this.sifraZaposlenog + ", ");
		super.prikaz(krajReda);
	}
}
