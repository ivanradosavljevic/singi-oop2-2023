# Primer 2
1. Napisati klasu kolekcija koja u sebi može da sadrži elemente bilo kojeg tipa. Omogućiti dodavanje i uklanjanje elementa iz kolekcije.
2. Dodati operator za indeksiranje elemenata u kolekciji.
3. Proveriti ispravnost implementacije.