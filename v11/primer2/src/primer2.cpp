#include <iostream>

#include "headers/Kolekcija.hpp"

using namespace std;

int main() {
	Kolekcija<double> *k = new Kolekcija<double>(10);
	k->add(10);
	k->add(20);

	for(int i = 0; i < k->length(); i++) {
		cout << (*k)[i] << endl;
	}
	return 0;
}
