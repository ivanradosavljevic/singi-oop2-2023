#ifndef HEADERS_PROFESOR_HPP_
#define HEADERS_PROFESOR_HPP_

#include "../headers/Osoba.hpp"
#include "../headers/Predmet.hpp"
#include <vector>
using namespace std;

class Predmet;

class Profesor : public Osoba {
private:
	vector<Predmet*> predaje;
public:
	Profesor();
	Profesor(string ime, string prezime, string jmbg);
	void dodajPredmet(Predmet *predmet, bool rekurzivno=true);
	void ukloniPredmet(Predmet *predmet, bool rekurzivno=true);
	virtual void detalji();
	virtual ~Profesor();
};

#endif /* HEADERS_PROFESOR_HPP_ */
