#ifndef HEADERS_REGISTAROCENA_HPP_
#define HEADERS_REGISTAROCENA_HPP_

#include "../headers/ProveraZnanja.hpp"
#include <vector>
using namespace std;

typedef vector<ProveraZnanja*> IzvodOcena;

class RegistarOcena {
private:
	vector<ProveraZnanja*> provere;
public:
	RegistarOcena();
	void dodajOcenu(ProveraZnanja *provera);
	void ukloniOcenu(ProveraZnanja *provera);
	IzvodOcena* operator [](string indeks);
	virtual ~RegistarOcena();
};



#endif /* HEADERS_REGISTAROCENA_HPP_ */
