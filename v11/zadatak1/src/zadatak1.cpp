#include <iostream>
using namespace std;

#include "headers/Predmet.hpp"
#include "headers/ProveraZnanja.hpp"
#include "headers/RegistarOcena.hpp"

int main() {
	RegistarOcena ro;
	ro.dodajOcenu(new ProveraZnanja(new Predmet("Predmet 1", "P1"), TipProvere::ISPIT, 10));
	ro.dodajOcenu(new ProveraZnanja(new Predmet("Predmet 1", "P1"), TipProvere::ISPIT, 3));
	ro.dodajOcenu(new ProveraZnanja(new Predmet("Predmet 1", "P2"), TipProvere::ISPIT, 2));
	ro.dodajOcenu(new ProveraZnanja(new Predmet("Predmet 1", "P1"), TipProvere::ISPIT, 5));

	cout << ro["P1"]->size() << endl;
	cout << ro["P2"]->size() << endl;
}
