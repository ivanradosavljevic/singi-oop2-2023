# Primer 1
1. Napisati klasu Vektor, kalsa vektor predstavlja kolekciju brojeva. Omogućiti dodavanje i uklanjanje brojeva.
2. Dodati operator za indeksiranje elemenata u vektoru.
3. Definisati operatore sabiranja i oduzimanja vektora. Rezultat primene ovih operatora je novi vektor.
4. Definisati operator poređenja dva vektora. Rezultat primene ovog operatora je boolean vrednost, true ukoliko vektori sadrže iste vrednosti, false u suprotnom.